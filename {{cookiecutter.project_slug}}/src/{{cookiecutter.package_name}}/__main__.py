"""
Provide a command line interface for {{cookiecutter.package_name}}.
"""

from {{cookiecutter.package_name}}.main import main

__all__ = ("main",)

if __name__ == "__main__":
    main()
