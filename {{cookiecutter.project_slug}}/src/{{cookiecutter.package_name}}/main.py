"""
Main entrypoint for {{cookiecutter.package_name}}.
"""

import argparse

import {{cookiecutter.package_name}}


def main():
    parser = argparse.ArgumentParser(
                        prog="{{cookiecutter.project_name}}",
                        description="{{cookiecutter.project_description}}")

    parser.add_argument("-V",
                        "--version",
                        action="store_true",
                        help="print package version")

    args = parser.parse_args()

    if args.version:
        print("{{cookiecutter.package_name}} " +
              {{cookiecutter.package_name}}.__version__)
    else:
        print("Hello from {{cookiecutter.package_name}}!")
