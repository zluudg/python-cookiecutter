import os
import sys
sys.path.insert(0, os.path.abspath("../src"))

import {{cookiecutter.package_name}}

project = '{{cookiecutter.project_name}}'
copyright = "{% now 'local', '%Y' %}, {{cookiecutter.commit_name}}"
author = '{{cookiecutter.commit_name}}'
release = '{{cookiecutter.package_name}}.__version__'
version = '{{cookiecutter.package_name}}.__version__'

extensions = ["sphinx.ext.autodoc", "sphinx.ext.viewcode"]
templates_path = ['_templates']
exclude_patterns = ["dist", ".tox", ".pytest_cache", "build", "venv"]

html_theme = 'alabaster'
html_static_path = ['_static']

nitpicky = True
