"""
Some basic sanity checks.
"""

import pytest


# Small import test first...
def test_import():
    """Test that the package can be imported."""
    with pytest.raises(NameError):
        {{cookiecutter.package_name}}
    import {{cookiecutter.package_name}}
    {{cookiecutter.package_name}}


# ...then properly import package and run the rest of the sanity tests
import {{cookiecutter.package_name}}


def test_has_author():
    """Test that the package has an author."""
    {{cookiecutter.package_name}}.__author__


def test_has_email():
    """Test that an email address is associated with the package."""
    {{cookiecutter.package_name}}.__email__


def test_has_version():
    """Test that the package has a version."""
    {{cookiecutter.package_name}}.__version__
