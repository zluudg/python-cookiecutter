import re
import sys


PKGNAME_REGEX = r'^_?[a-z][_a-z0-9]*[a-z0-9]$'

package_name = '{{cookiecutter.package_name}}'

if not re.match(PKGNAME_REGEX, package_name):
    print(f"ERROR: Package name '{package_name}' does not follow PEP 8!")
    sys.exit(1)
